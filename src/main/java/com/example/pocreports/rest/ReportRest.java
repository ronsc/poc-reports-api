package com.example.pocreports.rest;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.pocreports.dto.ReportDTO;
import com.googlecode.jthaipdf.jasperreports.engine.ThaiExporterManager;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@RestController
@RequestMapping(value = "/api/report")
@Slf4j
public class ReportRest implements Serializable {

	@GetMapping("/test-fonts")
	public ResponseEntity<?> getTestFonts(HttpServletResponse response) {
		try {
			InputStream jasperInput = getClass().getResourceAsStream("/reports/TEST_FONTS.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperInput, new HashMap<>(), new JREmptyDataSource());

			response.setContentType(MediaType.APPLICATION_PDF_VALUE);
			JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error(e.toString(), e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@GetMapping("/test-fonts-fix")
	public ResponseEntity<?> getTestFontsNoFixed(HttpServletResponse response) {
		try {
			InputStream jasperInput = getClass().getResourceAsStream("/reports/TEST_FONTS.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperInput, new HashMap<>(), new JREmptyDataSource());

			response.setContentType(MediaType.APPLICATION_PDF_VALUE);
			ThaiExporterManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error(e.toString(), e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PostMapping("/test-fonts-fix")
	public ResponseEntity<?> testFonts(@RequestBody ReportDTO dto, HttpServletResponse response) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("DTO", dto);

			InputStream jasperInput = getClass().getResourceAsStream("/reports/TEST_FONTS.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperInput, params, new JREmptyDataSource());

			response.setContentType(MediaType.APPLICATION_PDF_VALUE);
			// JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
			ThaiExporterManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error(e.toString(), e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
}
