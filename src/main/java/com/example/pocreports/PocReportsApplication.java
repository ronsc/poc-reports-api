package com.example.pocreports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocReportsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocReportsApplication.class, args);
	}

}
