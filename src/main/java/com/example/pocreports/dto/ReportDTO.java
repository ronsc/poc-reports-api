package com.example.pocreports.dto;

import lombok.Data;

@Data
public class ReportDTO {
	private String code;
	private String name;
}
